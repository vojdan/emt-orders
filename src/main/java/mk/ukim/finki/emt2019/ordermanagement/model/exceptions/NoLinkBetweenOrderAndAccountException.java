package mk.ukim.finki.emt2019.ordermanagement.model.exceptions;

/**
 * @author Riste Stojanov
 */
public class NoLinkBetweenOrderAndAccountException extends Exception {
}
