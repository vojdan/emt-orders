package mk.ukim.finki.emt2019.ordermanagement.repository.jpa;

import mk.ukim.finki.emt2019.ordermanagement.model.Quantity;
import mk.ukim.finki.emt2019.ordermanagement.model.ShoppingCart;
import mk.ukim.finki.emt2019.ordermanagement.model.projections.ShoppingCartExpiryProjection;
import mk.ukim.finki.emt2019.ordermanagement.service.view_model.CartTotalAndNumberOfItems;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

/**
 * @author Riste Stojanov
 */
public interface ShoppingCartRepository extends JpaRepository<ShoppingCart, Long>,
        JpaSpecificationExecutor<ShoppingCart> {


    @Query("SELECT oi.quantity " +
            "FROM ShoppingCart sc INNER JOIN sc.items oi " +
            "WHERE oi.product.productId=:productId")
    Stream<Quantity> getReservedQuantityForProduct(@Param("productId") Long productId);


    ShoppingCart save(ShoppingCart shoppingCart);

    ShoppingCart findByAccount_AccountNumber(String accountNumber);

    @EntityGraph(type = EntityGraph.EntityGraphType.FETCH,
            attributePaths = {"account", "items"})
    @Query("select c from ShoppingCart c where c.cartId=:cartId")
    Optional<ShoppingCart> fetchById(@Param("cartId") Long cartId);

    @EntityGraph(attributePaths = {"account", "items", "items.product"})
    Optional<ShoppingCart> findWithAccountAndItemsByCartId(Long cardId);


    /*
        The most important part of the query is '+0', it tries to greedily convert the `quantity` string to a double, up to the first non-digit character, which is fine for our use case since we are using | as separator.
     */
    @Query("select new mk.ukim.finki.emt2019.ordermanagement.service.view_model.CartTotalAndNumberOfItems(COUNT(ci), sum((ci.quantity+0)*1.0*ci.price)) from ShoppingCart c left join c.items ci where c.cartId=:cartId")
    Optional<CartTotalAndNumberOfItems> getCartStatus(@Param("cartId") Long cartId);

    /*
        Equivalent method to the findAllBy(ShoppingCartExpiryProjection.class) whose purpose is to show that when using
        projections in conjunction with native queries, no custom mappers are needed, only the parameter names should
        be the same.
     */
    @Query(value = "select cart_id as cartId, expiry_time as expiryTime from shopping_cart sc", nativeQuery = true)
    List<ShoppingCartExpiryProjection> getAllShoppingCartExpiryProjections();


    <T> List<T> findAllBy(Class<T> clazz);

    List<ShoppingCart> findAll(Specification<ShoppingCart> spec);

    List<ShoppingCartExpiryProjection> findByExpiryTimeAfter(LocalDateTime time);


}
