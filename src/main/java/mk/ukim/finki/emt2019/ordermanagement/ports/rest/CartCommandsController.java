package mk.ukim.finki.emt2019.ordermanagement.ports.rest;

import mk.ukim.finki.emt2019.ordermanagement.model.Quantity;
import mk.ukim.finki.emt2019.ordermanagement.model.ShoppingCart;
import mk.ukim.finki.emt2019.ordermanagement.model.exceptions.InvalidProductException;
import mk.ukim.finki.emt2019.ordermanagement.model.exceptions.InvalidQuantityException;
import mk.ukim.finki.emt2019.ordermanagement.model.exceptions.NotEnoughProductQuantityException;
import mk.ukim.finki.emt2019.ordermanagement.service.CartManagementService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

/**
 * @author Riste Stojanov
 */
@RestController
public class CartCommandsController {

    private final CartManagementService cartManagementService;

    public CartCommandsController(CartManagementService cartManagementService) {
        this.cartManagementService = cartManagementService;
    }

    @PostMapping("/add_item_to_new_cart")
    public ResponseEntity<Object> addItemToNewCart(@RequestParam Quantity quantity,
                                                   @RequestParam Long productId) throws InvalidQuantityException, NotEnoughProductQuantityException, InvalidProductException {
        ShoppingCart cart = this.cartManagementService.addItemToNewCart(quantity, productId);
        return ResponseEntity.created(ServletUriComponentsBuilder
                .fromCurrentContextPath()
                .path("/cart/{id}").build(cart.cartId)).build();

    }
}
