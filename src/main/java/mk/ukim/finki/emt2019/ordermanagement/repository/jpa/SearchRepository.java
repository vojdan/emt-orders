package mk.ukim.finki.emt2019.ordermanagement.repository.jpa;

import java.util.List;

/**
 * @author Riste Stojanov
 */
public interface SearchRepository {

  <T> List<T> searchKeyword(Class<T> entityClass, String text, String... fields);

  <T> List<T> searchPhrase(Class<T> entityClass,
                           String text,
                           String... fields);
}
