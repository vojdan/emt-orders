package mk.ukim.finki.emt2019.ordermanagement.model.events;

import mk.ukim.finki.emt2019.ordermanagement.model.ShoppingCart;

import java.time.LocalDateTime;

/**
 * @author Riste Stojanov
 */
public class CartCreatedEvent extends ShoppingCartEvent {

    public CartCreatedEvent(ShoppingCart source, LocalDateTime when) {
        super(source, when);
    }

    public CartCreatedEvent(ShoppingCart source) {
        super(source);
    }
}
