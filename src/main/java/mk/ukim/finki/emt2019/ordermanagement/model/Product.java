package mk.ukim.finki.emt2019.ordermanagement.model;

import org.apache.lucene.analysis.core.LowerCaseFilterFactory;
import org.apache.lucene.analysis.standard.StandardTokenizerFactory;
import org.hibernate.search.annotations.*;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

/**
 * @author Riste Stojanov
 */
@Indexed
@AnalyzerDef(name = "emtAnalyser",
        tokenizer = @TokenizerDef(
                factory = StandardTokenizerFactory.class
        ),
        filters = {
                @TokenFilterDef(factory = LowerCaseFilterFactory.class)
        })
@Entity
public class Product {

    @Id
    @NotNull
    public Long productId;

    @Field(index = org.hibernate.search.annotations.Index.YES,
            store = Store.NO,
            analyze = Analyze.YES)
    @Analyzer(definition = "emtAnalyser")
    @Boost(2f)
    public String displayName;

    public Double price;

    public Quantity stockQuantity;

    @IndexedEmbedded
    @ManyToOne
    public Category category;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Product product = (Product) o;

        return productId.equals(product.productId);
    }

    @Override
    public int hashCode() {
        return productId.hashCode();
    }
}
