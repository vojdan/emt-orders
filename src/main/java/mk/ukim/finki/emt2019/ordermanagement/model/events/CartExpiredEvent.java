package mk.ukim.finki.emt2019.ordermanagement.model.events;

import org.springframework.context.ApplicationEvent;

import java.time.LocalDateTime;

/**
 * @author Riste Stojanov
 */
public class CartExpiredEvent extends ApplicationEvent {

    private final LocalDateTime when;


    public CartExpiredEvent(Long cartId) {
        super(cartId);
        this.when = LocalDateTime.now();
    }

    public LocalDateTime getWhen() {
        return when;
    }

    public Long getCartId() {
        return (Long) source;
    }
}
