package mk.ukim.finki.emt2019.ordermanagement.service.view_model;

/**
 * @author Riste Stojanov
 */
public class CartTotalAndNumberOfItems {


    public CartTotalAndNumberOfItems(Long numberOfItems, Double totalPrice) {
        this.numberOfItems = numberOfItems;
        this.totalPrice = totalPrice;
    }

    public CartTotalAndNumberOfItems() {
    }

    public Long numberOfItems;

    public Double totalPrice;
}
