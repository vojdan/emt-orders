package mk.ukim.finki.emt2019.ordermanagement.model;

import org.hibernate.search.annotations.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author Riste Stojanov
 */
@Indexed
@Entity
public class Category {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long id;

    @Field(analyze = Analyze.YES,
            store = Store.NO,
            index = Index.YES)
    @Analyzer(definition = "emtAnalyser")
    public String name;
}
