package mk.ukim.finki.emt2019.ordermanagement.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;

/**
 * @author Riste Stojanov
 */


@EnableAsync
@Configuration
public class AsyncConfig {
}
