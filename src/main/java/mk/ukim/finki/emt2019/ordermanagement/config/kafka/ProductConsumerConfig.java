package mk.ukim.finki.emt2019.ordermanagement.config.kafka;

import mk.ukim.finki.emt2019.ordermanagement.model.Product;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;

/**
 * @author Riste Stojanov
 */
@Profile("kafka")
@Configuration
public class ProductConsumerConfig {

    private final GenericKafkaConsumerConfig consumerConfig;

    public ProductConsumerConfig(GenericKafkaConsumerConfig consumerConfig) {
        this.consumerConfig = consumerConfig;
    }


    @Bean("productsEventKafkaListenerFactory")
    public ConcurrentKafkaListenerContainerFactory<String, Product> kafkaListenerContainerFactory() {
        return consumerConfig.genericKafkaListenerFactory(Product.class);
    }

}
