package mk.ukim.finki.emt2019.ordermanagement.repository;

import mk.ukim.finki.emt2019.ordermanagement.model.ShoppingCart;

/**
 * @author Riste Stojanov
 */
public interface TxHelper {

    ShoppingCart readCartInTx(Long cartId);

    ShoppingCart fetchCartInTx(Long cartId);

    ShoppingCart fetchWithAttributesCartInTx(Long cartId);
}
